# README

This pipeline retrieves the genes from NCBI database. Align the sequence obtained. Generates trees and a Hidden Markov Model

This is pipeline has 4 steps:
1. Retrieving of genes and filtering by length and/or tax id
2. Multiple Sequence Alignment
3. Tree generation
4. Hidden Markov Model generation

Users need to have knowledge in `bash`, `python` and `R` programming.

## Initial setup

```sh
    git clone https://gitlab.com/NuriaV/nitro_curation
```
(You can also download the repository from the [Gitlab server](https://gitlab.com/NuriaV/nitro_curation)).

The directory nitro_curation will be downloaded. This folder has:

- README.md: this readme
- `scripts/`: this folder contains all the scripts of the pipeline
- `results/`: this folder contains the papers consulted, a brief summary of different genes involved in nitrogen cycle and tree figures obtained from nifH and narB

This pipeline uses some programs that have to be installed:

- [Biopython](https://biopython.org/)
- [prodigal](https://github.com/hyattpd/Prodigal)
- [SeqKit](https://bioinf.shenwei.me/seqkit/)
- [mafft](https://mafft.cbrc.jp/alignment/software/)
- [fasttree](http://www.microbesonline.org/fasttree/)
- [PREQUAL](https://github.com/simonwhelan/prequal)
- [HMMER](http://hmmer.org/)

R libraries:

- ggplot2 version 3.1.1
- ggtree version 1.16.1
- treeio version 1.8.1

## 1. Retrieving of genes and filtering by length

This step is made by `01_retrieval_genes.sh` script. Uses Biopython, prodigal, SeqKit and PREQUAL.<br/>
Depending on the flag can do different things:

- Download from a file of gene ids or accession numbers of NCBI. Ex: gene id: 3719198 / accession number: NZ_SNVI01000008.1
- Download from names of genes. Ex: nifH
- Both together

The use one of this options depends of the flag in the arguments.<br/>
It needs the PREQUAL path so, each user has to write inside the script the PREQUAL path in the variable PREQUAL_PATH (it is in the first uncommented line).<br/>
There is an option to filter by tax id (only the data obtained from flag -name is filtered)<br/>
There is a part that a sequences from a paper are added (uses script 02_fasta2table.py in script/helpers folder). This part has to be changed depending on the sequences that you had and the taxonomy. Once the script is changed you have to put the file with the sequences in `data/gene_name/genedb/fasta/(gene_name)_literature_genes.fa`

## 2. Multiple Sequence Alignment

This step is made by `02_align_seqs.sh` script. Uses mafft.<br/>
This script creates two alignments one with the nucleotide sequences and another with the aminoacid sequences retrieved from `01_retrieval_genes.sh` script.

## 3. Tree generation

This step is made by `03_tree_generation.sh` script. Uses fasttree.<br/>
This script generate trees one for each multiple sequence alignment file that are obtained from `02_align_seqs.sh` script. Then creates the tree figures.

## 4. Hidden Markov Model generation

This step is made by `04_hmm.sh` script. Uses HMMER.<br/>
This script generates HMM from the protein alignment obtained from `02_align_seqs.sh` script. Then generates a plot with the E values obtained and calculate a threshold.<br/>
This script needs a file with the outgroup sequences. This file has to be in the working directory (nitro_curation) `outgroup_(gene_name).fa`. gene_name is the name of the gene that it is studied. 

## Running the whole pipeline

One thing to take into account is that when a file is read, the last line is not read. So, every file read has to have a newline at the end. <br/>
Depending on which type of retrieval you want the arguments are a bit different:

#### Downloading genes from name:
```sh
./run_workflow.sh -name gene_name min_length vis_taxonomy flag_tax file_tax_ids
```
gene_name is the name of the gene wanted.<br/>
min_length is the variable that filters the sequences and this is the minimum length.<br/>
vis_taxonomy has to be phylum or class. It will be by which of the two the tree branches are coloured.<br/>
The two for tax are not compulsory, only if you want to filter by tax: <br/>
flag_tax is -tax.<br/>
file_tax is a file with the tax ids that you want.<br/>

#### Downloading genes from ids (the ids have to be of the same gene):
```sh
./run_workflow.sh -id gene_name file_with_ids min_length
```
gene_name id the gene name of the ids.<br/>
file_with_ids is a file that contains the ids of the gene wanted (one each line).<br/>
min_length is the variable that filters the sequences and this is the minimum length.<br/>

#### Downloading from both options:
```sh
./run_workflow.sh -id gene_name_id file_with_ids -name gene_name_gb min_length vis_taxonomy flag_tax file_tax
```
gene_name_id is the gene name of the ids.<br/>
file_with_ids is a file that contains the ids of the gene wanted (one each line).<br/>
gene_name_gb is the name of the gene that the sequences will be downloaded from NCBI.<br/>
min_length is the variable that filters the sequences and this is the minimum length.<br/>
vis_taxonomy has to be phylum or class. It will be by which of the two the tree branches are coloured.<br/>
The two for tax are not compulsory, only if you want to filter by tax: <br/>
flag_tax is -tax.<br/>
file_tax is a file with the tax ids that you want.<br/>
