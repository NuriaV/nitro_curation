#!/bin/bash

#This script makes a hmm with hmmbuild
#Then adds outgroup (has to be aa) to the aa sequences filtered
#and creates a table with ids and the source (GenBank, Paper or Outgroup)
#Then with this fasta sequences does a hmmsearch
#extract the table with E values and it makes a histogram colored by the source
#the command for first and second option:
#./scripts/04_hmm.sh (1 or 2) gene_name(file or name)
#the command for the third option:
#./scripts/04_hmm.sh 3 gene_name gene_name_file


flag="$1"
name="$2"

if [[ $flag = "1" ]] || [[ $flag = "2" ]]; then
	echo "Creating the hmm"
	hmmbuild data/${name}/hmm/${name}.hmm data/${name}/alignments/${name}_aa_align.fasta
	echo "Adding outgroup"
	python3 scripts/helpers/08_outgroup2table.py outgroup_${name}.fa \
	data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered
	cat outgroup_${name}.fa >> data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered
	echo "Doing hmmsearch with the outgroup and the sequences"
	hmmsearch --tblout data/${name}/hmm/${name}_hmm_table.tsv \
		--noali data/${name}/hmm/${name}.hmm data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered \
		> data/${name}/hmm/${name}.out
	python3 scripts/helpers/09_reform_hmm_table.py data/${name}/hmm/${name}_hmm_table.tsv
	Rscript scripts/helpers/10_hist_e_value.R data/${name}/hmm/${name}_hmm_table.tsv \
	data/${name}/hmm/source_table.tsv results/figures/${name}/${name}_hist_eval.png
else
	name_gb="$3";
	echo "Creating the hmm"
	hmmbuild data/${name}/hmm/${name}.hmm data/${name}/alignments/${name}_aa_align.fasta
	echo "Adding outgroup"
	python3 scripts/helpers/08_outgroup2table.py outgroup_${name}.fa \
	data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered
	cat outgroup_${name}.fa >> data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered
	echo "Doing hmmsearch with the outgroup and the sequences"
	hmmsearch --tblout data/${name}/hmm/${name}_hmm_table.tsv \
		--noali data/${name}/hmm/${name}.hmm data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered \
		> data/${name}/hmm/${name}.out
	python3 scripts/helpers/09_reform_hmm_table.py data/${name}/hmm/${name}_hmm_table.tsv
	Rscript scripts/helpers/10_hist_e_value.R data/${name}/hmm/${name}_hmm_table.tsv \
	data/${name}/hmm/source_table.tsv results/figures/${name}/${name}_hist_eval.png
	if [[ ! -f data/${name_gb}/tree/${name_gb}_aa_tree.txt && \
		! -f data/${name_gb}/tree/${name_gb}_seq_tree.txt ]]; then
		echo "Creating the hmm"
		hmmbuild data/${name_gb}/hmm/${name_gb}.hmm data/${name_gb}/alignments/${name_gb}_aa_align.fasta
		echo "Adding outgroup"
		python3 scripts/helpers/08_outgroup2table.py outgroup_${name_gb}.fa \
		data/${name_gb}/genedb/fasta/${name_gb}_aa_nodup.fa.filtered
		cat outgroup_${name_gb}.fa >> data/${name_gb}/genedb/fasta/${name_gb}_aa_nodup.fa.filtered
		echo "Doing hmmsearch with the outgroup and the sequences"
		hmmsearch --tblout data/${name_gb}/hmm/${name_gb}_hmm_table.tsv \
			--noali data/${name_gb}/hmm/${name_gb}.hmm data/${name_gb}/genedb/fasta/${name_gb}_aa_nodup.fa.filtered \
			> data/${name_gb}/hmm/${name_gb}.out
		python3 scripts/helpers/09_reform_hmm_table.py data/${name_gb}/hmm/${name_gb}_hmm_table.tsv
		Rscript scripts/helpers/10_hist_e_value.R data/${name_gb}/hmm/${name_gb}_hmm_table.tsv \
		data/${name_gb}/hmm/source_table.tsv results/figures/${name_gb}/${name_gb}_hist_eval.png
	fi;
fi;