#!/bin/bash

#First or the third option:
#This script generates a tree from a multiple sequence alignment and 4 images.
#Two for seq, one coloured by source the other coloured by tax.
#The other two images are the same but with aa.

#Second option:
#This script generates a tree from a multiple sequence alignment and 2 images.
#The two coloured by source but one with seq and the other with aa.


#Command for running (from nitro_curation folder):
#From names:
#./scripts/03_tree_generation.sh 1 gene_name vis_tax(phylum or class)
#From ids:
#./scripts/03_tree_generation.sh 2 gene_name
#From both:
#./scripts/03_tree_generation.sh 3 gene_name file_with_gene_names vis_tax(phylum or class)

flag="$1"
name="$2"


if [[ $flag = "1" ]]; then
	vis_tax="$3"
	echo "Generating gene tree of" $name
	fasttree -gtr -nt data/${name}/alignments/${name}_seq_align.fasta > data/${name}/tree/${name}_seq_tree.txt
	echo "Generating protein tree of" $name
	fasttree data/${name}/alignments/${name}_aa_align.fasta > data/${name}/tree/${name}_aa_tree.txt
	echo "Obtaining the complete taxonomy for each sequence"
	python3 scripts/helpers/11_get_taxonomy.py data/${name}/genedb/${name}_table.tsv
	echo "Creating an image of the trees and saved in results/figures folder"
	Rscript scripts/helpers/07_visualising_trees.R data/${name}/tree/${name}_seq_tree.txt \
	data/${name}/genedb/${name}_table.tsv data/${name}/genedb/taxids.csv \
	results/figures/${name}/${name}_seq_source_tree.png results/figures/${name}/${name}_seq_tax_tree.png ${vis_tax,,}
	Rscript scripts/helpers/07_visualising_trees.R data/${name}/tree/${name}_aa_tree.txt \
	data/${name}/genedb/${name}_table.tsv data/${name}/genedb/taxids.csv \
	results/figures/${name}/${name}_aa_source_tree.png results/figures/${name}/${name}_aa_tax_tree.png ${vis_tax,,}
	echo "Trees finished"
else
	if [[ $flag = "2" ]]; then
		echo "Generating gene tree of" $name
		fasttree -gtr -nt data/${name}/alignments/${name}_seq_align.fasta > data/${name}/tree/${name}_seq_tree.txt
		echo "Generating protein tree of" $name
		fasttree data/${name}/alignments/${name}_aa_align.fasta > data/${name}/tree/${name}_aa_tree.txt
		echo "Creating an image of the trees and saved in results/figures folder"
		Rscript scripts/helpers/07_visualising_trees.R data/${name}/tree/${name}_seq_tree.txt \
		data/${name}/genedb/${name}_table.tsv data/${name}/genedb/taxids.csv \
		results/figures/${name}/${name}_seq_source_tree.png results/figures/${name}/${name}_seq_tax_tree.png ${vis_tax,,}
		Rscript scripts/helpers/07_visualising_trees.R data/${name}/tree/${name}_aa_tree.txt \
		data/${name}/genedb/${name}_table.tsv data/${name}/genedb/taxids.csv \
		results/figures/${name}/${name}_aa_source_tree.png results/figures/${name}/${name}_aa_tax_tree.png ${vis_tax,,}
		echo "	Trees finished"
	else
		if [[ $flag = "3" ]]; then
			name_gb="$3";
			vis_tax="$4"
			echo "Generating gene tree of" $name
			fasttree -gtr -nt data/${name}/alignments/${name}_seq_align.fasta > data/${name}/tree/${name}_seq_tree.txt
			echo "Generating protein tree of" $name
			fasttree data/${name}/alignments/${name}_aa_align.fasta > data/${name}/tree/${name}_aa_tree.txt
			echo "Obtaining the complete taxonomy for each sequence"
			python3 scripts/helpers/11_get_taxonomy.py data/${name}/genedb/${name}_table.tsv
			echo "Creating an image of the trees and saved in results/figures folder"
			Rscript scripts/helpers/07_visualising_trees.R data/${name}/tree/${name}_seq_tree.txt \
			data/${name}/genedb/${name}_table.tsv data/${name}/genedb/taxids.csv \
			results/figures/${name}/${name}_seq_source_tree.png results/figures/${name}/${name}_seq_tax_tree.png ${vis_tax,,}
			Rscript scripts/helpers/07_visualising_trees.R data/${name}/tree/${name}_aa_tree.txt \
			data/${name}/genedb/${name}_table.tsv data/${name}/genedb/taxids.csv \
			results/figures/${name}/${name}_aa_source_tree.png results/figures/${name}/${name}_aa_tax_tree.png ${vis_tax,,}
			echo "	Trees finished"
			if [[ ! -f data/${name_gb}/tree/${name_gb}_aa_tree.txt && \
				! -f data/${name_gb}/tree/${name_gb}_seq_tree.txt ]]; then
				echo "Generating gene tree of" $name_gb
				fasttree -gtr -nt data/${name_gb}/alignments/${name_gb}_seq_align.fasta > data/${name_gb}/tree/${name_gb}_seq_tree.txt
				echo "Generating protein tree of" $name_gb
				fasttree data/${name_gb}/alignments/${name_gb}_aa_align.fasta > data/${name_gb}/tree/${name_gb}_aa_tree.txt
				echo "Obtaining the complete taxonomy for each sequence"
				python3 scripts/helpers/11_get_taxonomy.py data/${name_gb}/genedb/${name_gb}_table.tsv
				echo "Creating an image of the trees and saved in results/figures folder"
				Rscript scripts/helpers/07_visualising_trees.R data/${name_gb}/tree/${name_gb}_seq_tree.txt \
				data/${name_gb}/genedb/${name_gb}_table.tsv data/${name_gb}/genedb/taxids.csv \
				results/figures/${name_gb}/${name_gb}_seq_source_tree.png results/figures/${name_gb}/${name_gb}_seq_tax_tree.png ${vis_tax,,}
				Rscript scripts/helpers/07_visualising_trees.R data/${name_gb}/tree/${name_gb}_aa_tree.txt \
				data/${name_gb}/genedb/${name_gb}_table.tsv data/${name_gb}/genedb/taxids.csv \
				results/figures/${name_gb}/${name_gb}_aa_source_tree.png results/figures/${name_gb}/${name_gb}_aa_tax_tree.png ${vis_tax,,}
				echo "	Trees finished"
			fi;
		fi;
	fi;
fi;