#This script will read a file with one taxid
#in each line and then search in the table
#the sequences that have the given taxids
#and erase the others
from Bio import SeqIO
import sys
import re
import csv

fasta_file=sys.argv[1]
table=sys.argv[2]
ftax=sys.argv[3]
seq={}

def read_table(table):
	seq_id={}
	with open(table, "r") as table:
		reader = csv.reader(table, delimiter='\t')
		for line in reader:
			try:
				seq_id[line[0]]=line[7]
			except:
				pass
	return seq_id

id_tax=read_table(table)
with open(ftax, "r") as ft:
	for tax in ft:
		for seq_record in SeqIO.parse(fasta_file, "fasta"):
			if seq_record.id in id_tax:
				if int(id_tax[seq_record.id])==int(tax):
					seq[seq_record.id]=str(seq_record.seq)

with open(fasta_file, "w") as f:
	for s in seq:
		f.write(">" + s + "\n" + seq[s] + "\n")
