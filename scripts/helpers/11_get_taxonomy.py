#Code extracted from https://stackoverflow.com/questions/36503042/how-to-get-taxonomic-specific-ids-for-kingdom-phylum-class-order-family-gen

#This script takes each taxid and search its phylum, class, order, family, genus, species and subspecies.
#Then put the name (not the id) of each one of this ranks and put in a table.

from Bio import SeqIO
import csv
import sys
import re
from ete3 import NCBITaxa

ncbi = NCBITaxa()

table = sys.argv[1] #input file
g=re.match( r'(\S*)/(\w*)_table.tsv', table)
path_table = g.group(1) #path to the table

def get_desired_ranks(taxid, desired_ranks):
	ranks2names={}
	lineage = ncbi.get_lineage(taxid)
	lineage2names = ncbi.get_taxid_translator(lineage) #get the names of the taxonomy
	lineage2ranks = ncbi.get_rank(lineage) #get the ranks
	for r in lineage2ranks:
		for n in lineage2names:
			if r==n:
				ranks2names[lineage2ranks[r]]=lineage2names[n] #put the rank with its name for each taxid
	return {'{}_names'.format(rank): ranks2names.get(rank, 'NA') for rank in desired_ranks}

def main(taxids, desired_ranks, path):
	with open(path, 'w') as csvfile:
		fieldnames = ['{}_names'.format(rank) for rank in desired_ranks]
		csvfile.write("new_id"+"\t")
		writer = csv.DictWriter(csvfile, delimiter='\t', fieldnames=fieldnames)
		writer.writeheader()
		i=0
		for taxid in taxids:
			if taxid != "NA":
				csvfile.write(ids[i]+"\t")
				writer.writerow(get_desired_ranks(taxid, desired_ranks))
				i+=1
			else:
				csvfile.write(ids[i]+"\t"+"NA"+"\t"+"NA"+"\t"+"NA"+"\t"+"NA"+"\t"+"NA"+"\t"+"NA"+"\t"+"NA"+"\n")
				i+=1

if __name__ == '__main__':
	ids=[]
	taxids=[]
	with open(table, "r") as table:
		reader = csv.reader(table, delimiter='\t')
		next(reader, None) #skip the headers
		for line in reader:
			ids.append(str(line[0]))
			taxids.append(line[7])
	desired_ranks = ['phylum', 'class', 'order', 'family', 'genus', 'species', 'subspecies']
	path = path_table+'/taxids.csv'
	main(taxids, desired_ranks, path)