#This script filters the sequences by a given min_length
#I used part of the script of this web page: https://biopython.org/wiki/Sequence_Cleaner

from Bio import SeqIO
import sys
import re

fasta_file=sys.argv[1] #input fasta file
min_length=sys.argv[2] #argument of the minimum length
g=re.match( r'(\S*)/(\w*)_(\w*)', fasta_file)
filtered=g.group(1)+"/"+g.group(2)+"_"+g.group(3)+"_filtered_"+min_length+".fa" #output file

# Create our hash table to add the sequences
sequences={}
ids=[]


# Using the Biopython fasta parse we can read our fasta input
for seq_record in SeqIO.parse(fasta_file, "fasta"):
    # Take the current sequence
    sequence = str(seq_record.seq).upper()
    # Check if the current sequence is according to the user parameters
    if int(len(sequence)) >= int(min_length):
    # If the sequence passed in the test "is it clean?" and it isn't in the
    # hash table, the sequence and its id are going to be in the hash
        if sequence not in sequences and seq_record.id not in ids:
            sequences[sequence] = seq_record.id
            ids.append(seq_record.id)

# Create a file in the same directory where you ran this script
with open(filtered, "a") as output_file:
    # Just read the hash table and write on the file as a fasta format
    for sequence in sequences:
        output_file.write(">" + sequences[sequence] + "\n" + sequence + "\n")
