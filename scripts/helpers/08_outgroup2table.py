#This script takes a file with the outgroup sequences
#and the table with the information of the sequences studied
#then creates another table that it has the new id (a new id
#for the outgroup sequences is generated) and the source. 
from Bio import SeqIO
import sys
import re

outgroup_file = sys.argv[1] #input file
g=re.match( r'(\S*)_(\w*).', outgroup_file) #to extract the name of the gene
source_table = "data/"+str(g.group(2))+"/hmm/source_table.tsv" #output file (table)
fasta_file = sys.argv[2] #input file
i=1
seq={}

with open(source_table, "w") as t:
	t.write("id"+"\t"+"source"+"\n")
	for rec in SeqIO.parse(open(fasta_file,"r"), "fasta"):
		if str(g.group(2)) in rec.id: #if it has the new id
			t.write(rec.id+"\t"+"GenBank"+"\n")
		else: #if it not has the new id
			t.write(rec.id+"\t"+"Paper"+"\n")
	for out in SeqIO.parse(open(outgroup_file,"r"), "fasta"):
		new_id="Outgroup"+str(i)
		t.write(new_id+"\t"+"Outgroup"+"\n")
		seq[new_id]=str(out.seq)
		i+=1

with open(outgroup_file, "w") as o:
	for s in seq:
		o.write(">" + s + "\n" + seq[s] + "\n")