#I used part of the program of Sanjay kumar Srikakulam
#(https://www.researchgate.net/profile/Sanjay_Kumar_Srikakulam/post/How_can_I_parse_a_GenBank_file_to_retrieve_specific_gene_sequences_with_IDs/attachment/5b17ea7a4cde260d15e392b2/AS%3A634523565973506%401528293615280/download/gen_bank_parser.py)

#This script takes a GeneBank file and extract the unique id,
#the length of the nucleotide sequence, where starts and finish,
#and the organism and its taxid. In the column of source it is written GenBank 
#Then this information is put in the file table. 
from Bio import SeqIO
import sys
import re

input_file = sys.argv[1] #input file
g=re.match( r'(\S*)/(\w*).', input_file) #to extract the path and the name of the gene
table = str(g.group(1))+"/"+str(g.group(2))+"_table.tsv" #output file (table)
i=1

headers=["new_id","Unique_ID","length_seq","start","end","source","taxonomy","taxid"]

with open(table, "a") as table:
	table.write("\t".join(headers)+"\n")
	for rec in SeqIO.parse(open(input_file,"r"), "gb"):
		uid=rec.id
		for feature in rec.features:
			for key, val in feature.qualifiers.items():
					if key=='organism':
						org=val[0]
					if key=='db_xref':
						t=re.match(r'(\S*):(\w*)', val[0])
						if t.group(1)=='taxon':
							taxon=t.group(2)
					if feature.type!='gene':
						if str(g.group(2)) in val:
							start=feature.location.nofuzzy_start
							end=feature.location.nofuzzy_end
							seq=rec.seq[start:end]
							new_id=str(g.group(2))+str(i)
							table.write(new_id+"\t"+uid+"\t"+str(end-start)+"\t"+str(start)+"\t"+
								str(end)+"\t"+"GenBank"+"\t"+org+"\t"+taxon+"\n")
							i+=1