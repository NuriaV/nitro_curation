#This script takes the id of a fasta sequence and
#if it is not the new_id then it changes by new_id.
#then take the sequence and put it in one line and
#erase every thing that are not letters.
#All it is rewritten in the same file.

from Bio import SeqIO
import sys
import re
import csv


fasta_file=sys.argv[1]
table=sys.argv[2]
seq={}

def read_table(table):
    seq_id={}
    with open(table, "r") as table:
        reader = csv.reader(table, delimiter='\t')
        for line in reader:
            seq_id[line[1]]=line[0]
    return seq_id

for seq_record in SeqIO.parse(fasta_file, "fasta"):
        if ":" in seq_record.id:
            uid=re.match( r'(\S*):', seq_record.id)
            sequence = str(seq_record.seq).upper()
            sequence= re.sub(r'[^\w]',"",sequence)
            seq_id=read_table(table)
            if uid.group(1) in seq_id:
                seq[seq_id[uid.group(1)]]=sequence
                
        elif "_1" in seq_record.id:
            uid=re.match( r'(\S*)_1$', seq_record.id)
            sequence = str(seq_record.seq).upper()
            sequence= re.sub(r'[^\w]',"",sequence)
            seq[uid.group(1)]=sequence
       
        elif "." in seq_record.id:
            seq_id=read_table(table)
            sequence = str(seq_record.seq).upper()
            sequence= re.sub(r'[^\w]',"",sequence)
            seq[seq_id[seq_record.id]]=sequence            
        
        else:
            sequence = str(seq_record.seq).upper()
            sequence= re.sub(r'[^\w]',"",sequence)
            seq[seq_record.id]=sequence

with open(fasta_file, "w") as f:
    for s in seq:
        f.write(">" + s + "\n" + str(seq[s]) + "\n")
