#This script extract the id and the length of the sequence
#from the fasta file downloaded. Then creates a new id.
#If the table is not created the script will create one and
#put the new id, the id, the length of the sequence 
#and in source GenBank.
#If the table it is created will write bellow what it is inside
#and will follow the numeration of the new id from
#the last line in the table.
from Bio import SeqIO
import os
import sys
import re

input_file = sys.argv[1] #input file
g=re.match( r'(\S*)/(\w*)/(\w*)_(\w*)_', input_file) #to extract the path and the name of the gene
table = str(g.group(1))+"/"+str(g.group(3))+"_table.tsv" #output file (table)
i=1

if not os.path.exists(table):
	with open(table, "w") as t:
		t.write("new_id"+"\t"+"Unique_ID"+"\t"+"length_seq"+"\t"+"start"+"\t"+"end"+
			"\t"+"source"+"\t"+"taxonomy"+"\t"+"taxid"+"\n")
		for rec in SeqIO.parse(open(input_file,"r"), "fasta"):
			if ":" in rec.id:
				uid=re.match( r'(\S*):', rec.id)
				new_id=str(g.group(3))+str(i)
				t.write(new_id+"\t"+uid.group(1)+"\t"+str(len(rec.seq))+"\t"+"\t"+"\t"+"GenBank"+"\t"+"\t"+"NA"+"\n")
				i+=1
			else:
				new_id=str(g.group(3))+str(i)
				t.write(new_id+"\t"+rec.id+"\t"+str(len(rec.seq))+"\t"+"\t"+"\t"+"GenBank"+"\t"+"\t"+"NA"+"\n")
				i+=1
else:
	with open(table, "r") as t:
		i = sum(1 for line in t)
	with open(table, "a") as t:
		for rec in SeqIO.parse(open(input_file,"r"), "fasta"):
			if ":" in rec.id:
				uid=re.match( r'(\S*):', rec.id)
				new_id=str(g.group(3))+str(i)
				t.write(new_id+"\t"+uid.group(1)+"\t"+str(len(rec.seq))+"\t"+"\t"+"\t"+"GenBank"+"\t"+"\t"+"NA"+"\n")
				i+=1
			else:
				new_id=str(g.group(3))+str(i)
				t.write(new_id+"\t"+rec.id+"\t"+str(len(rec.seq))+"\t"+"\t"+"\t"+"GenBank"+"\t"+"\t"+"NA"+"\n")
				i+=1
