#This script takes the output table obtained from hmmbuild
#and rewrites it to a tabular form and erase the non necessary lines
import sys
import re

table=sys.argv[1]

headers=["accession","query name","accession","E-value",
		"score","bias","E-value","score","bias","exp","reg","clu",
		"ov","env","dom","rep","inc","description of target"]

with open(table, "r") as f:
	lines=f.readlines()

with open(table, "w") as t:
	t.write("\t"+"\t".join(headers)+"\n")
	for line in lines:
		if "#" in line.strip("\n"):
			pass
		else:
			t.write("\t".join(line.split()))
			t.write("\n")