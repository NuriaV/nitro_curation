#This script take the sequences downloaded from a paper and then
#include the uids, sequence length, the source and the taxonomy
#for each sequence in the table
from Bio import SeqIO
import sys
import re 

input_file = sys.argv[1] #input file
g=re.match( r'(\S*)/(\w*)/(\w*)_(\w*)_', input_file) #to extract the path and the name of the gene
table = str(g.group(1))+"/"+str(g.group(3))+"_table.tsv" #output file

with open(table, "a") as table:
	for rec in SeqIO.parse(open(input_file,"r"), "fasta"):
		if "HBD01" in rec.id:
			org="Desulfovibrio"
			tax=872
		elif "HBD02" in rec.id:
			org="Desulfobacterales"
			tax=213118
		elif "HBD03" in rec.id or "HBD04" in rec.id or "HBD05" in rec.id:
			org="Oceanospirillales"
			tax=135619
		elif "HBD06" in rec.id or "HBD07" in rec.id:
			org="Pseudomonadales"
			tax=72274
		elif "HBD08" in rec.id or "HBD09" in rec.id:
			org="Planctomycetes"
			tax=203682
		else:
			org=''
			tax="NA"
		table.write(rec.id+"\t"+rec.id+"\t"+str(len(rec.seq))+"\t"+"\t"+"\t"+"Paper"+"\t"+org+"\t"+str(tax)+"\n")
