#!/bin/bash

#This script is the main one.
#Depending the option that you want you need different arguments (if they are incorrect an error message will appear):
#For the first option (-name):
#It is needed the flag -name (that we will be transformed to 1),
#a file with the names of the genes that you want,
#the minimal length of the sequences that you want to study,
#the path where the prequal program is,
#and if you want that the phylogenetic tree is coloured by phylum or class.
#Then the flag_tax (-tax) and a file with taxids which you want to filter are optional.
#The command will be (from nitro_curation folder):
#./run_workflow -name file_gene_names min_len vis_tax (flag_tax file_tax)


#For the second option (-id):
#It is needed the flag -id (that we will be transformed to 2),
#the name of the gene that you want,
#a file with the ids,
#the minimal length of the sequences that you want to study,
#the path where the prequal program is.
#The command will be (from nitro_curation folder):
#./run_workflow -id gene_name file_with_ids min_len


#For the third option (-id -name):
#It is needed the flag -id (that we will be transformed to 3),
#the name of the gene that you want,
#a file with the ids,
#then the flag -name,
#a file with the names of the genes that you want,
#the minimal length of the sequences that you want to study,
#the path where the prequal program is,
#and if you want that the phylogenetic tree is coloured by phylum or class.
#Then the flag_tax (-tax) and a file with taxids which you want to filter are optional.
#The command will be (from nitro_curation folder):
#./run_workflow -id gene_name file_with_ids -name file_gene_names min_len vis_tax (flag_tax file_tax)

flag=$1
name=$2

if [[ "$#" -ge 4 && "$#" -le 6 && $flag = "-name" ]]; then
	flag="1"
	min_len=$3
	vis_tax=$4
	flag_tax=$5
	file_tax=$6
	./scripts/01_retrieval_genes.sh $flag $name $min_len $flag_tax $file_tax
	./scripts/02_align_seqs.sh $flag $name
	./scripts/03_tree_generation.sh $flag $name $vis_tax 
	./scripts/04_hmm.sh $flag $name
else
	if [[ "$#" = 4 && $flag = "-id" ]]; then
		flag="2"
		file=$3
		min_len=$4
		./scripts/01_retrieval_genes.sh $flag $name $file $min_len 
		./scripts/02_align_seqs.sh $flag $name
		./scripts/03_tree_generation.sh $flag $name
		./scripts/04_hmm.sh $flag $name
	else
		if [[ "$#" -ge 7 && "$#" -le 9 && $flag = "-id" ]]; then
			flag="3"
			file=$3
			flag2=$4
			gene_name_gb=$5
			min_len=$6
			vis_tax=$7
			flag_tax=$8
			file_tax=$9
			./scripts/01_retrieval_genes.sh $flag $name $file $flag2 $gene_name_gb $min_len $flag_tax $file_tax
			./scripts/02_align_seqs.sh $flag $name $gene_name_gb
			./scripts/03_tree_generation.sh $flag $name $gene_name_gb $vis_tax
			./scripts/04_hmm.sh $flag $name $gene_name_gb
		else
			echo "Wrong number of inputs or wrong order"
			echo "Possible inputs: (The ones between brackets are optional)"
			echo "  ./run_workflow -name gene_name min_len vis_tax (flag_tax file_tax)"
			echo "  ./run_workflow -id gene_name file_with_ids min_len"
			echo "  ./run_workflow -id gene_name file_with_ids -name gene_name min_len vis_tax (flag_tax file_tax)"
		fi;
	fi;
fi;