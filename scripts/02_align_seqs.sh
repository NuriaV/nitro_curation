#!/bin/bash

#Command for running (from nitro_curation folder):
#From ids and from names
#./scripts/02_align_seqs.sh flag gene_names
#From both:
#./scripts/02_align_seqs.sh flag gene_name_ids gene_name_gb

flag="$1"
name="$2"

if [[ $flag = "1"  ]] || [[ $flag = "2" ]]; then
	echo "Aligning" $name "gene" 
	mafft data/${name}/genedb/fasta/${name}_seq_prodigal.fa \
	> data/${name}/alignments/${name}_seq_align.fasta
	echo "Aligning" $name "protein"
	mafft data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered \
	> data/${name}/alignments/${name}_aa_align.fasta
	echo "Alignments finished"

else
	name_gb="$3";
	echo "Aligning" $name "gene"
	mafft data/${name}/genedb/fasta/${name}_seq_prodigal.fa \
	> data/${name}/alignments/${name}_seq_align.fasta
	echo "Aligning" $name "protein"
	mafft data/${name}/genedb/fasta/${name}_aa_nodup.fa.filtered\
	> data/${name}/alignments/${name}_aa_align.fasta
	echo "Alignments finished"
	if [[ ! -f data/${name_gb}/alignments/${name_gb}_aa_align.fa && \
		! -f data/${name_gb}/alignments/${name_gb}_seq_align.fa ]]; then
			echo "Aligning" $name_gb "gene" 
			mafft data/${name}/genedb/fasta/${name_gb}_seq_prodigal.fa \
			> data/${name}/alignments/${name_gb}_seq_align.fasta
			echo "Aligning" $name_gb "protein"
			mafft data/${name_gb}/genedb/fasta/${name_gb}_aa_nodup.fa.filtered \
			> data/${name_gb}/alignments/${name_gb}_aa_align.fasta
			echo "Alignments finished"
	fi;
fi;