#!/bin/bash

#This script in this part with the flag -name
#takes the name of the gene
#creates the folders
#and returns a GenBank file (.gb) for each gene name.
#to read the file with gene names, the command has to be like:
#./01_retrieval_genes.sh 1 file_with_gene_names min_length

#with the flag -id takes a file
#with the ncbi accession number or gene id (one per line)
#creates the folders
#and returns a file with fasta sequence from each id.
#to read the file with ids, the command has to be like:
#./01_retrieval_genes.sh 2 gene_name file_with_ids min_length


#with the two flags, creates the folders
#download the fasta sequence from each id
#and then returns a GenBank file (.gb) for gene name.
#the command has to be like:
#./01_retrieval_genes.sh 3 gene_name file_with_ids -name file_with_gene_names min_length

#Here you have to change the "~" with your PREQUAL path:
PREQUAL_PATH="~/prequal"

echo "Creating the folders"
echo "Searching genbank file. This could take time"

if [[ "$1" = "1" ]]; then
	gene_name=$2;
	min_len=$3;
	flag_tax=$4;
	file_tax=$5;
	genes=()
	mkdir -p data/${gene_name}/alignments
	mkdir -p data/${gene_name}/genedb/fasta
	mkdir -p data/${gene_name}/tree
	mkdir -p data/${gene_name}/hmm
	mkdir -p results/figures/${gene_name}
	esearch -db Nucleotide \
  	-query "$gene_name[Gene name] AND (bacteria[filter]) NOT (uncultured[title]) AND srcdb_refseq[PROP]" | \
	efetch -format gbwithparts > data/${gene_name}/genedb/${gene_name}.gb;
	genes+=(${gene_name});
	
fi;

if [[ "$1" = "2" ]]; then
	gene_name=$2;
	id_file=$3;
	min_len=$4;
	genes=();
	mkdir -p data/${gene_name}/alignments
	mkdir -p data/${gene_name}/genedb/fasta
	mkdir -p data/${gene_name}/tree
	mkdir -p data/${gene_name}/hmm
	mkdir -p results/figures/${gene_name}
	echo "Downloading from ids of " $gene_name
	esearch -db Nucleotide \
   	-query $(paste -s -d ','  $id_file) | \
	efetch -format fasta > data/${gene_name}/genedb/fasta/${gene_name}_seq_ids.fa;
	genes+=(${gene_name});
fi;

if [[ "$1" = "3" ]]; then
	gene_name_id=$2;
	id_file=$3;
	flag2=$4;
	gene_name_gb=$5;
	min_len=$6;
	flag_tax=$7;
	file_tax=$8;
	genes=();
	genes+=(${gene_name_id});
	mkdir -p data/${gene_name_id}/alignments
	mkdir -p data/${gene_name_id}/genedb/fasta
	mkdir -p data/${gene_name_id}/tree
	mkdir -p data/${gene_name_id}/hmm
	mkdir -p results/figures/${gene_name_id}
	echo "Downloading from ids of" $gene_name_id
	esearch -db Nucleotide \
    -query $(paste -s -d ','  $id_file) | \
	efetch -format fasta > data/${gene_name_id}/genedb/fasta/${gene_name_id}_seq_ids.fa;
	echo "Downloading " $gene_name_gb;
	mkdir -p data/${gene_name_gb}/alignments
	mkdir -p data/${gene_name_gb}/genedb/fasta
	mkdir -p data/${gene_name_gb}/tree
	mkdir -p data/${gene_name_gb}/hmm
	mkdir -p results/figures/${gene_name_gb}
	esearch -db Nucleotide \
  	-query "$gene_name_gb[Gene name] AND (bacteria[filter]) NOT (uncultured[title]) AND srcdb_refseq[PROP]" | \
	efetch -format gbwithparts > data/${gene_name_gb}/genedb/${gene_name_gb}.gb;
	if [[ $gene_name_id != $gene_name_gb ]];then
		genes+=(${gene_name_gb})
	fi;
	
fi;

#If the first or the third option are run:
#Creates a table (.tsv) with the information downloaded before.
#Table contains a new id, id, length of the sequence, start position,
#end position,the source, taxonomy and taxid.
#From the table extract the id, the start and the end position
#and download from GenBank the sequence in fasta format for the gb file


#If the second or the third option are run (this last one have the two things)
#Creates a table (.tsv) but only with the information that can be extracted
#form the sequences.


#For all options:
#All fasta sequences are changed its id for the new id
#Filters the sequences and only takes the ones that have
#the length that you want or more.
#Then you can filter by taxid if you want
#Then translates the filtered sequences to aa.
#The duplicated aa sequences are removed
#this aa sequences are filtered by prequal

echo "Retreving both table and fasta sequences"
for i in ${genes[@]}; do
	echo "	Gene:" $i;

	if [ -f data/${i}/genedb/${i}.gb ]; then
		echo "Creating the table"
		python3 scripts/helpers/01_gb2table.py data/${i}/genedb/${i}.gb;
		echo "Table finished"
		echo "Downloading sequences"
		while read new_id Unique_ID length_seq start end source taxonomy taxid
		do
	   		[ "$Unique_ID" == "Unique_ID" ] && continue
	        efetch -db Nucleotide -format fasta -id $Unique_ID \
	    	-chr_start $start     -chr_stop $end

		done < data/${i}/genedb/${i}_table.tsv > data/${i}/genedb/fasta/${i}_seq.fa;
		python3 scripts/helpers/03_fasta_reform.py data/${i}/genedb/fasta/${i}_seq.fa \
		data/${i}/genedb/${i}_table.tsv
		echo "Sequences downloaded"
		if [[ $flag_tax = "-tax" ]]; then
			echo "Filtering by taxid"
			python3 scripts/helpers/06_filter_taxid.py data/${i}/genedb/fasta/${i}_seq.fa \
			data/${i}/genedb/${i}_table.tsv $file_tax
			echo "Filtering finished"
		fi;

	fi;

	if [ -f  data/${i}/genedb/fasta/${i}_seq_ids.fa ]; then
		echo "Creating/adding to the table"
		python3 scripts/helpers/05_id2table.py data/${i}/genedb/fasta/${i}_seq_ids.fa;
		python3 scripts/helpers/03_fasta_reform.py data/${i}/genedb/fasta/${i}_seq_ids.fa \
		data/${i}/genedb/${i}_table.tsv
		if [ -f data/${i}/genedb/fasta/${i}_seq.fa ]; then
			cat data/${i}/genedb/fasta/${i}_seq_ids.fa >> data/${i}/genedb/fasta/${i}_seq.fa
		else
			mv data/${i}/genedb/fasta/${i}_seq_ids.fa data/${i}/genedb/fasta/${i}_seq.fa
		fi;

	fi;

	if [ -f data/${i}/genedb/fasta/${i}_literature_genes.fa ]; then
		echo "Adding literature genes to the table"
		python3 scripts/helpers/03_fasta_reform.py data/${i}/genedb/fasta/${i}_literature_genes.fa \
		data/${i}/genedb/${i}_table.tsv
		#This step is specific for the paper used, the script 02_fasta2table.py has to be changed
		#depending on how you have the fasta (names, taxonomy,...)
		python3 scripts/helpers/02_fasta2table.py data/${i}/genedb/fasta/${i}_literature_genes.fa
		cat data/${i}/genedb/fasta/${i}_literature_genes.fa >> data/${i}/genedb/fasta/${i}_seq.fa
	fi;

	echo ""
	echo "	Number of sequences:" `grep -c ">" data/${i}/genedb/fasta/${i}_seq.fa`
	echo ""
	echo "Filtering sequences by length"
	python3 scripts/helpers/04_clean_seq.py data/${i}/genedb/fasta/${i}_seq.fa $min_len;
	echo ""
	echo "	Number of sequences after filtering by length:" \
	`grep -c ">" data/${i}/genedb/fasta/${i}_seq_filtered_${min_len}.fa `
	echo ""
	echo "Translating sequences"
	prodigal -i data/${i}/genedb/fasta/${i}_seq_filtered_${min_len}.fa \
	-a data/${i}/genedb/fasta/${i}_aa.fa -d data/${i}/genedb/fasta/${i}_seq_prodigal.fa \
	-p meta -o data/${i}/genedb/output_prodigal.txt -q;
	echo ""
	echo "	Number of protein sequences:" `grep -c ">" data/${i}/genedb/fasta/${i}_aa.fa`
	echo ""
	python3 scripts/helpers/03_fasta_reform.py data/${i}/genedb/fasta/${i}_aa.fa \
	data/${i}/genedb/${i}_table.tsv
	python3 scripts/helpers/03_fasta_reform.py data/${i}/genedb/fasta/${i}_seq_prodigal.fa \
	data/${i}/genedb/${i}_table.tsv
	echo "Removing aa sequences duplicated"
	seqkit rmdup -s data/${i}/genedb/fasta/${i}_aa.fa > data/${i}/genedb/fasta/${i}_aa_nodup.fa;
	echo ""
	echo "	Number of protein sequences after removing duplicates:" `grep -c ">" data/${i}/genedb/fasta/${i}_aa_nodup.fa`
	echo ""
	echo "Filtering aa with PREQUAL"
	${PREQUAL_PATH} -nodna data/${i}/genedb/fasta/${i}_aa_nodup.fa;
	echo "Finished the filtering of sequences"
	echo ""
	echo "	Number of protein sequences after PREQUAL" `grep -c ">" data/${i}/genedb/${i}_aa_nodup.fa.filtered`
	echo ""
done;