# Web pages and papers consulted

### Links:

  - [Searching Uncultivated Bacteria and Archaea (UBA) Genomes for Important Genes](https://tylerbarnum.wordpress.com/2018/06/22/searching-uncultivated-bacteria-and-archaea-uba-genomes-for-important-genes/)
  - [Phylogenetic diversity of nitrogen-utilizing genes in hydrothermal chimneys from 3 middle ocean ridges](https://link.springer.com/article/10.1007/s00792-015-0788-4)
  - [Construction and Screening of Marine Metagenomic Large Insert Libraries](https://link.springer.com/protocol/10.1007/978-1-4939-6691-2_3)
  - [Rapid annotation of nifH gene sequences using classification and regression trees facilitates environmental functional gene analysis](https://onlinelibrary.wiley.com/doi/full/10.1111/1758-2229.12455)
  - [UCYN‐A3, a newly characterized open ocean sublineage of the symbiotic N2‐fixing cyanobacterium Candidatus Atelocyanobacterium thalassa](https://onlinelibrary.wiley.com/doi/full/10.1111/1462-2920.14429)
  - [Nitrogen-fixing populations of Planctomycetes and Proteobacteria are abundant in surface ocean metagenomes](https://www.nature.com/articles/s41564-018-0176-9)
  - [Phylogenetic Diversity of Nitrogenase (nifH) Genes in Deep-Sea and Hydrothermal Vent Environments of the Juan de Fuca Ridge](https://aem.asm.org/content/69/2/960.full)
  - [Distribution of nitrogen fixation and nitrogenase-like sequences amongst microbial genomes](https://bmcgenomics.biomedcentral.com/articles/10.1186/1471-2164-13-162#Decs)
  - [Trichodesmium - a widespread marine cyanobacterium with unusual nitrogen fixation properties](https://academic.oup.com/femsre/article/37/3/286/583761)
  \
  \
  \
  - [ARBitrator: a software pipeline for on-demand retrieval of auto-curated nifH sequences from GenBank](https://academic.oup.com/bioinformatics/article/30/20/2883/2422235)

# Databases

### Links:
  
  - [nifH Database Public](https://wwwzehr.pmc.ucsc.edu/nifH_Database_Public/)
